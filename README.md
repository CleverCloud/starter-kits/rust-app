# Rust application starter for Clever Cloud

> :wave: :warning: :construction: work in progress

[![pipeline status](https://gitlab.com/CleverCloud/starter-kits/rust-app/badges/master/pipeline.svg)](https://gitlab.com/CleverCloud/starter-kits/rust-app/commits/master)

## What?

This project is a starter kit to create and deploy easily an **Iron** application on [Clever Cloud](https://www.clever-cloud.com/en/).

Since the moment you fork this project you can deploy the application on **Clever Cloud** and create [reviews applications](https://about.gitlab.com/features/review-apps/)

## Setup

- First, clone this project or fork this project (then go to the **General Settings** in the **Advanced** section and **Remove fork relationship**)
- Add these 4 environment variables to the CI/CD settings of your project (or group)
  - `CLEVER_SECRET`
  - `CLEVER_TOKEN`
  - `ORGANIZATION_ID`
  - `ORGANIZATION_NAME` *(optional)*

> - To get `CLEVER_SECRET` and `CLEVER_TOKEN`, you need to install the [Clever Tools CLI](https://github.com/CleverCloud/clever-tools) and then you have to [login](https://github.com/CleverCloud/clever-tools#how-to-use) with this command `clever login`. It will open a page in your browser. Copy the provided token and secret in the **Variables** section of the CI/CD settings
> - You can find the `ORGANIZATION_ID` in the Clever Cloud web administration console ([https://console.clever-cloud.com/](https://console.clever-cloud.com/)).

## Update the `.gitlab-ci.yml`

Change the values of these 2 variables with your values

```yaml
variables:
  CLEVER_APPLICATION_NAME: "rust-app-demo"
  CLEVER_APPLICATION_DOMAIN: "rust-app-demo"
```

## Thanks

- :sparkles: :sparkles: :sparkles: to [@hsablonniere](https://gitlab.com/hsablonniere) for his :construction: on the `.gitlab-ci.yml` file

## Disclaimer :wave:

We provide this kit for educational purposes. It is subject to an opensource license. You can modify them according to your needs. These projects are not part of the Clever Cloud support. However, if you need help do not hesitate to create an issue in the associated project: **[Create an issue](https://gitlab.com/CleverCloud/starter-kits/rust-app/issues)**.


